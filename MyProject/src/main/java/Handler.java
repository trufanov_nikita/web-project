import java.util.Objects;

public class Handler {
	private static int counter;
	private int id;
	private boolean work;
	private String shef;

	public Handler(String name, boolean status) {
		id = ++counter;
		shef = name;
		work = status;
	}

	public int getId() {
		return id;
	}
	
	public boolean getWork() {
		return work;
	}
	public void workAdd() {
		work = true;
	}
	public void workDel() {
		work = false;
	}
	public String getShef() {
		return shef;
	}
	@Override
	public String toString() {
		return shef;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Handler other = (Handler) obj;
		return id == other.id;
	}
}
