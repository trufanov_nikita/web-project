
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/add_ticket")
public class CommandAddTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String htmText = "<!DOCTYPE html>\r\n"
			+ "<html lang=\"en\">\r\n"
			+ "<head>\r\n"
			+ "	<meta charset=\"UTF-8\">\r\n"
			+ "	<title>My test</title>\r\n"
			+ "	<link rel=\"stylesheet\" href=\"./styles/styles1.css\">\r\n"
			+ "    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\r\n"
			+ "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\r\n"
			+ "<link href=\"https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap\" rel=\"stylesheet\">\r\n"
			+ "    <link href=\"http://fonts.cdnfonts.com/css/roboto\" rel=\"stylesheet\">\r\n"
			+ "    <script src=\"./script1.js\" defer></script>\r\n"
			+ "</head>\r\n"
			+ "\r\n"
			+ "<body>\r\n"
			+ "    <header class=\"header\">\r\n"
			+ "        <nav class=\"menu\">\r\n"
			+ "            <div class=\"menu-wrapper\">\r\n"
			+ "                <div class=\"menu-logo\">\r\n"
			+ "                    <i class=\"logo\"></i>\r\n"
			+ "                    <span class=\"name\">Deliver company</span>\r\n"
			+ "                </div>\r\n"
			+ "                <div class=\"dropdown\">\r\n"
			+ "                    <span class=\"reg\">Order Complite Your shef is shef-name-change</span>\r\n"
			+ "                </div>\r\n"
			+ "                <div class=\"dropdown\">\r\n"
			+ "                    <span class=\"reg\">Gongradulation</span>\r\n"
			+ "                </div>\r\n"
			+ "            </div>\r\n"
			+ "        </nav>\r\n"
			+ "    </header>\r\n"
			+ "    <div class=\"page-wrapper\">\r\n"
			+ "        <div class=\"shef-wrapper\" id=\"shef-wrapper\">\r\n"
			+ "            <span class=\"shef-name\" id=\"shef-name\">shefs-phrase</span>\r\n"
			+ "            \r\n"
			+ "            <img class=\"shef-photo\" id=\"shef-img\" src=\"shef-photo-change\" alt=\"shef-img\">\r\n"
			+ "            <span class=\"shef-name\" id=\"shef-description\">shefs-place</span>\r\n"
			+ "        </div>\r\n"
			+ "       \r\n"
			+ "        <div class=\"food-wrapper\" id=\"food-wrapper\">\r\n"
			+ "            <span class=\"food\" id=\"food-name\">food-name-change</span>\r\n"
			+ "            <img class=\"food-photo\" id=\"food-img\" src=\"food-photo-change\"></img>\r\n"
			+ "            <span class=\"food\" id=\"food-description\">food-description-change</span>\r\n"
			+ "        </div>\r\n"
			+ "\r\n"
			+ "    </div>\r\n"
			+ "</body>\r\n"
			+ "</html>	";

//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		String result = "";
//		ServiceDesk serviceDesk = ServiceDesk.getInsatnce();
//		Ticket ticket = serviceDesk.addTicket();
//		result = "ticket: " + ticket;
//		response.setContentType("text/plain;charset=UTF-8");
//		response.getWriter().write(result);
//	}
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		doGet(request, response);
//	}
//
//}

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServiceDesk serviceDesk = ServiceDesk.getInsatnce();
		
		System.out.println("Test.doGet()");
		
		String number = request.getParameter("phone");
		String name = request.getParameter("name");
		String choice = request.getParameter("choice");
		int tic = serviceDesk.checkPhone(number);

		if (serviceDesk.checkShef("Ratatui") == 0) {
			serviceDesk.addHandler("Ratatui");
		}
		if (serviceDesk.checkShef("Splinter") == 0) {
			serviceDesk.addHandler("Splinter");
		}
		if (serviceDesk.checkShef("Tom") == 0) {
			serviceDesk.addHandler("Tom");
		}

		String htm1 = htmText;
		if (tic != 1) {
			Ticket ticket = serviceDesk.addTicket(number, name, choice);
			String result = "";
			String china = "china";
			String france = "france";
			String italy = "italy";
			String htm2 = "";
			
			System.out.println(choice);
			if (choice.equals(france))
			{
				if(serviceDesk.handl("Ratatui", ticket.getId()) == null) {
					htmText = htmText.replace("Gongradulation", "Please return to the main page");
					htmText = htmText.replace("Order Complite Your shef is", "Ops");
					htmText = htmText.replace("shef-name-change", "Ratatui is busy");
					htmText = htmText.replace("shefs-phrase", "Dont worry Krisa make the best Ratatui for you");
					htmText = htmText.replace("shefs-place", "France");
					htmText = htmText.replace("shef-photo-change", "./Assets/France.jfif");
					htmText = htmText.replace("food-name-change", "Ratatui");
					htmText = htmText.replace("food-description-change", "Ratatui");
					htmText = htmText.replace("food-photo-change", "./Assets/Ratatui.jfif");
				}
				else {
					htmText = htmText.replace("shef-name-change", "Ratatui?");
					htmText = htmText.replace("shefs-phrase", "Dont worry Krisa make the best Ratatui for you");
					htmText = htmText.replace("shefs-place", "France");
					htmText = htmText.replace("shef-photo-change", "./Assets/France.jfif");
					htmText = htmText.replace("food-name-change", "Ratatui");
					htmText = htmText.replace("food-description-change", "Ratatui");
					htmText = htmText.replace("food-photo-change", "./Assets/Ratatui.jfif");
				}
			}
			if (choice.equals(italy))
			{
				if(serviceDesk.handl("Splinter", ticket.getId()) == null) {
					htmText = htmText.replace("Gongradulation", "Please return to the main page");
					htmText = htmText.replace("Order Complite Your shef is", "Ops");
					htmText = htmText.replace("shefs-phrase", "Oh, you really want pizza from krisa");
					htmText = htmText.replace("shefs-place", "Good choice");
					htmText = htmText.replace("shef-photo-change", "./Assets/italy-shef.jfif");
					htmText = htmText.replace("food-name-change", "Piza");
					htmText = htmText.replace("food-description-change", "Piiiiiza");
					htmText = htmText.replace("food-photo-change", "./Assets/italy-food.jfif");
							
				}
				else {
					htmText = htmText.replace("shef-name-change", "Splinter");
					htmText = htmText.replace("shefs-phrase", "Oh, you really want pizza from krisa");
					htmText = htmText.replace("shefs-place", "Good choice");
					htmText = htmText.replace("shef-photo-change", "./Assets/italy-shef.jfif");
					htmText = htmText.replace("food-name-change", "Piza");
					htmText = htmText.replace("food-description-change", "Piiiiiza");
					htmText = htmText.replace("food-photo-change", "./Assets/italy-food.jfif");
				}
			}
			if (choice.equals(china))
			{
				if(serviceDesk.handl("Tom", ticket.getId()) == null) {
					htmText = htmText.replace("Gongradulation", "Please return to the main page");
					htmText = htmText.replace("Order Complite Your shef is", "Ops");
					htmText = htmText.replace("shef-name-change", "Tom is busy");
					htmText = htmText.replace("shefs-phrase", "Tom will make your sushi in the cleanest way");
					htmText = htmText.replace("shefs-place", "Good choice");
					htmText = htmText.replace("shef-photo-change", "./Assets/china-shef.jfif");
					htmText = htmText.replace("food-name-change", "Sushi");
					htmText = htmText.replace("food-description-change", "Sushi");
					htmText = htmText.replace("food-photo-change", "./Assets/china-food.jfif");
							
				}
				else {
					htmText = htmText.replace("shef-name-change", "Tom");
					htmText = htmText.replace("shefs-phrase", "Tom will make your sushi in the cleanest way");
					htmText = htmText.replace("shefs-place", "Good choice");
					htmText = htmText.replace("shef-photo-change", "./Assets/china-shef.jfif");
					htmText = htmText.replace("food-name-change", "Sushi");
					htmText = htmText.replace("food-description-change", "Sushi");
					htmText = htmText.replace("food-photo-change", "./Assets/china-food.jfif");
				}
			}
			
			result = "ticket: " + ticket;
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(htmText);
			htmText = htm1;
		}
		else {
			htmText = htmText.replace("Gongradulation", "Please return to the main page");
			htmText = htmText.replace("Order Complite Your shef is shef-name-change", "Ops we already have your order");
			htmText = htmText.replace("shef-name-change", "Tom");
			htmText = htmText.replace("shefs-phrase", "Tom will make your sushi in the cleanest way");
			htmText = htmText.replace("shefs-place", "Good choice");
			htmText = htmText.replace("shef-photo-change", "./Assets/china-shef.jfif");
			htmText = htmText.replace("food-name-change", "Sushi");
			htmText = htmText.replace("food-description-change", "Sushi");
			htmText = htmText.replace("food-photo-change", "./Assets/china-food.jfif");
			response.getWriter().write(htmText);
			htmText = htm1;		
			}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Test.doPost()");
		
		doGet(request, response);
		
	}
	
}