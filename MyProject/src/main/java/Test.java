
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String htmText = "<!DOCTYPE html>\r\n"
			+ "<html lang=\"en\">\r\n"
			+ "<head>\r\n"
			+ "	<meta charset=\"UTF-8\">\r\n"
			+ "	<title>My test</title>\r\n"
			+ "	<link rel=\"stylesheet\" href=\"./styles/styles.css\">\r\n"
			+ "    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\r\n"
			+ "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\r\n"
			+ "<link href=\"https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap\" rel=\"stylesheet\">\r\n"
			+ "    <link href=\"http://fonts.cdnfonts.com/css/roboto\" rel=\"stylesheet\">\r\n"
			+ "    <script src=\"./script.js\" defer></script>\r\n"
			+ "</head>\r\n"
			+ "\r\n"
			+ "<body>\r\n"
			+ "    <header class=\"header\">\r\n"
			+ "        <nav class=\"menu\">\r\n"
			+ "            <div class=\"menu-wrapper\">\r\n"
			+ "                <div class=\"menu-logo\">\r\n"
			+ "                    <i class=\"logo\"></i>\r\n"
			+ "                    <span class=\"name\">Deliver company</span>\r\n"
			+ "                </div>\r\n"
			+ "                <div class=\"dropdown\">\r\n"
			+ "                    <button class=\"dropbtn\" style=\"width: 50vh;\">Order</button>\r\n"
			+ "                    <div class=\"dropdown-order\">\r\n"
			+ "                    <form action = \"add_ticket\" method=\"POST\">\r\n"
			+ "                        <a href=\"#\"><label class=\"form-label\" for=\"name\">Name</label>\r\n"
			+ "                            <input class=\"form-input\" type=\"text\" id=\"name\" name=\"name\" placeholder=\"Your Name\" required></a>\r\n"
			+ "                        <a href=\"#\"><label class=\"form-label\" for=\"name\">e-mail</label>\r\n"
			+ "                            <input class=\"form-input\" type=\"text\" id=\"name\" name=\"e=mail\" placeholder=\"Your e-mail\" required></a>\r\n"
			+ "                        \r\n"
			+ "                        <a href=\"#\"><label class=\"form-label\" for=\"phone\" >Phone</label>\r\n"
			+ "                            <input class=\"form-input\" type=\"tel\" id=\"phone\" name=\"phone\" pattern=\"[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}\" placeholder=\"0992505590\" required></a>\r\n"
			+ "                        <a href=\"#\"><label class=\"form-label\" for=\"select\">Food</label>\r\n"
			+ "                            <select class=\"form-input form-input-select\"name=\"choice\" id=\"select\"  required >\r\n"
			+ "                                \r\n"
			+ "                                <option value=\"italy\">Pizza</option>\r\n"
			+ "                                <option value=\"france\" selected>Ratatui</option>\r\n"
			+ "                                <option value=\"china\">Sushi</option>\r\n"
			+ "                               \r\n"
			+ "                            </select></a> \r\n"
			+ "                           \r\n"
			+ "                        <a href=\"#\"> <button class=\"form-submit-button\" type=\"submit\" >Register order</button></a>\r\n"
			+ "                    </form>\r\n"
			+ "                    </div>\r\n"
			+ "                    \r\n"
			+ "                </div>\r\n"
			+ "                <div class=\"dropdown\">\r\n"
			+ "                    <button class=\"dropbtn\">MENU</button>\r\n"
			+ "                    <div class=\"dropdown-content\">\r\n"
			+ "                        <span class=\"menu-text\"  id=\"menu-text\">\r\n"
			+ "                            <a href = \"#\" id =\"china\">Chineese</a>\r\n"
			+ "                            <a href = \"#\" id = \"france\">French</a>\r\n"
			+ "                            <a href = \"#\" id = \"italy\">Italian</a> \r\n"
			+ "                         </span>   \r\n"
			+ "                    </div>\r\n"
			+ "                </div>\r\n"
			+ "            </div>\r\n"
			+ "        </nav>\r\n"
			+ "    </header>\r\n"
			+ "    <div class=\"page-wrapper\">\r\n"
			+ "        <div class=\"shef-wrapper\" id=\"shef-wrapper\">\r\n"
			+ "            <span class=\"shef-name\" id=\"shef-name\">Krisa from France with love</span>\r\n"
			+ "            \r\n"
			+ "            <img class=\"shef-photo\" id=\"shef-img\" src=\"./Assets/France.jpg\" alt=\"shef-img\">\r\n"
			+ "            <span class=\"shef-name\" id=\"shef-description\">Best shef of France</span>\r\n"
			+ "        </div>\r\n"
			+ "        <div class=\"secondary-photo\">\r\n"
			+ "            <div class=\"china-secondary\">\r\n"
			+ "\r\n"
			+ "                <div class=\"secondary-text\" id=\"secondary-text\">Sushi</div>\r\n"
			+ "                <img src=\"./Assets/china-food.jpg\" id=\"secondary-photo\"alt=\"secondary-photo\" class=\"secondary\" >\r\n"
			+ "            </div>\r\n"
			+ "            <div class=\"france-secondary\">\r\n"
			+ "                <div class=\"secondary-text\" id=\"secondary-text1\">Ratatui</div>\r\n"
			+ "                <img src=\"./Assets/Ratatui.jfif\" id=\"secondary-photo1\"alt=\"secondary-photo1\" class=\"secondary\" >\r\n"
			+ "            </div>\r\n"
			+ "            <div class=\"italy-secondary\">\r\n"
			+ "                <div class=\"secondary-text\" id=\"secondary-text2\">Pizza</div>\r\n"
			+ "                <img src=\"./Assets/italy-food.jpg\" id=\"secondary-photo2\"alt=\"secondary-photo2\" class=\"secondary\" >\r\n"
			+ "            </div>\r\n"
			+ "        </div>\r\n"
			+ "        <div class=\"food-wrapper\" id=\"food-wrapper\">\r\n"
			+ "            <span class=\"food\" id=\"food-name\">Ratatui</span>\r\n"
			+ "            <img class=\"food-photo\" id=\"food-img\" src=\"./Assets/Ratatui.jfif\"></img>\r\n"
			+ "            <span class=\"food\" id=\"food-description\">Ratatui</span>\r\n"
			+ "        </div>\r\n"
			+ "\r\n"
			+ "    </div>\r\n"
			+ "</body>\r\n"
			+ "</html>	";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Test.doGet()");
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(htmText.replace("$$$", "Something"));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Test.doPost()");
		doGet(request, response);
	}

}