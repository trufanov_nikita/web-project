public class Ticket {
	enum Status {
		NEW, INPROGRESS, CLOSED
	}

	private static int counter;
	private int id;
	private String number;
	private String name;
	private String choice;
	private Handler handler;
	private Status status = Status.NEW;

	public Ticket(String phone, String myName, String myChoice) {
		id = ++Ticket.counter;
		number = phone;
		name = myName;
		choice = myChoice;
	}

	public int getId() {
		return id;
	}

	public Handler getHandler() {
		return handler;
	}

	public Status status() {
		return status;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getName() {
		return name;
	}
	
	public String getChoice() {
		return choice;
	}
	
	@Override
	public String toString() {
		return "Ticket #" + id + " "+ name + " "+ number + " "+ choice +" "+status + " " + handler;
	}

	public void handle(Handler newHandler) {
		handler = newHandler;
		status = Status.INPROGRESS;
	}
	
	
	public void close() {
		status = Status.CLOSED;
	}
}
