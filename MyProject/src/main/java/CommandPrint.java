
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.security.Escape;

import java.lang.String;
import com.google.gson.Gson;

@WebServlet("/print")
public class CommandPrint extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String htmText = "<!DOCTYPE html>\r\n"
			+ "<html lang=\"en\">\r\n"
			+ "<head>\r\n"
			+ "	<meta charset=\"UTF-8\">\r\n"
			+ "	<title>My test</title>\r\n"
			+ "	<link rel=\"stylesheet\" href=\"./styles/styles.css\">\r\n"
			+ "    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\r\n"
			+ "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\r\n"
			+ "<link href=\"https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap\" rel=\"stylesheet\">\r\n"
			+ "    <link href=\"http://fonts.cdnfonts.com/css/roboto\" rel=\"stylesheet\">\r\n"
			+ "    <script src=\"./script.js\" defer></script>\r\n"
			+ "</head>\r\n"
			+ "\r\n"
			+ "<body>\r\n"
			+ "    <header class=\"header\">\r\n"
			+ "        <nav class=\"menu\">\r\n"
			+ "            <div class=\"menu-wrapper\">\r\n"
			+ "                <div class=\"menu-logo\">\r\n"
			+ "                    <i class=\"logo\"></i>\r\n"
			+ "                    <span class=\"name\">Deliver company</span>\r\n"
			+ "                </div>\r\n"
			+ "                \r\n"
			+ "                <div class=\"dropdown\">\r\n"
			+ "                    <span class=\"dropbtn\">All your query</span>\r\n"
			+ "                </div>\r\n"
			+ "                <div class=\"dropdown\">\r\n"
			+ "                    <span class=\"dropbtn\">\\-0-/</span>\r\n"
			+ "                </div>\r\n"
			+ "            </div>\r\n"
			+ "        </nav>\r\n"
			+ "    </header>\r\n"
			+"<div class=\"tickets\">@@@</div>"
			+ "</body>\r\n"
			+ "</html>	";
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServiceDesk serviceDesk = ServiceDesk.getInsatnce();
		StringBuilder data= new StringBuilder(600);
		for(Ticket ticket : serviceDesk.getTickets()) {
			data.append("<div class=\"ticket\">");
			data.append("<span class=\"ticket_name\">");
			data.append("<label class=\"label\">Ticket id: </label>");
			data.append(Escape.htmlElementContent(ticket.getId()));
			data.append("<br/>");
			data.append("<label class=\"label\">Phone: </label>");
			data.append(Escape.htmlElementContent(ticket.getNumber()));
			data.append("<br/>");
			data.append("<label class=\"label\">Name: </label>");
			data.append(Escape.htmlElementContent(ticket.getName()));
			data.append("<br/>");
			data.append("<label class=\"label\">Order: </label>");
			data.append(Escape.htmlElementContent(ticket.getChoice()));
			data.append("<br/>");
			data.append("<label class=\"label\">Handler: </label>");
			data.append(Escape.htmlElementContent(ticket.getHandler()));
			data.append("<br/>");
			data.append("<label class=\"label\">Status: </label>");
			data.append(Escape.htmlElementContent(ticket.status()));
			data.append("</span>");
			data.append("</div>");
		}
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(htmText.replace("@@@", data.toString() ));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
