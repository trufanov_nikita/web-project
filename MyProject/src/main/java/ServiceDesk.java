import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceDesk {
	private List<Ticket> tickets = new ArrayList<Ticket>();

	private List<Handler> handlers = new ArrayList<Handler>();

	private static ServiceDesk instance;

	private ServiceDesk() {
		super();
	}

	public static ServiceDesk getInsatnce() {
		if (instance == null) {
			instance = new ServiceDesk();
		}
		return instance;
	}

	public Handler addHandler(String name) {
		Handler handler = new Handler(name, false);
		handlers.add(handler);
		System.out.println("ServiceDesk.addHandler(), " + handler);
		return handler;
	}



	public List<Handler> handlers() {
		return handlers;
	}

	public Ticket addTicket(String phone, String name, String choice) {		
		Ticket ticket = new Ticket(phone, name, choice);
		tickets.add(ticket);
		System.out.println("ServiceDesk.add(), " + ticket);
		return ticket;
	}
	public Ticket getTicket(String phone, String name, String choice) {		
		Ticket ticket = new Ticket(phone, name, choice);
		return ticket;
	}
	
	

	public void close(Handler handler) {
		for (Ticket ticket : tickets) {
			if (ticket.getHandler() == handler && ticket.status() == Ticket.Status.INPROGRESS) {
				ticket.close();
				handler.workDel();
				break;
			}
		}

		System.out.println("ServiceDesk.close(), " + handler);
	}

	public List<Ticket> tickets(Ticket.Status status) {
		return tickets.stream().filter(x -> x.status() == status).collect(Collectors.toList());
	}

	public List<Ticket> tickets(Handler handler) {
		return tickets.stream().filter(x -> x.getHandler() == handler && x.status() == Ticket.Status.INPROGRESS)
				.collect(Collectors.toList());
	}

	public  int checkPhone(String phone) {
		int result = 0;
		for(Ticket tick : tickets) {
			if(phone.equals(tick.getNumber())) {
				result = 1;
			}
		}
		return result;
	}
	
	public int checkShef(String name) {
		int result = 0;
		for (Handler hand : handlers) {
			if(name.equals(hand.getShef())) {
				result = 1;
			}
		}
		return result;
	}
	public void print() {
		System.out.println("Tickets:");
		if (tickets == null || tickets.size() == 0) {
			System.out.println("<EMPTY>");
		} else {
			for (Ticket ticket : tickets) {
				System.out.println(ticket);
			}
		}
		System.out.println("---");
	}

	public Handler handle(Handler handler) {
		Handler result = null;
		if (handler != null) {
			for (Ticket ticket : tickets) {
				if (ticket.status() == Ticket.Status.NEW) {
					ticket.handle(handler);
					result = handler;
					break;
				}
			}
		}

		System.out.println("ServiceDesk.handle(), " + handler);
		return result;
	}
	public List<Ticket> getTickets() {
		
		return tickets;
	}
	public Handler handl(String name, int Ticketid) {
		Handler result = null;
		for(Handler hand : handlers) {
			if(name.equals(hand.getShef())) {
				System.out.println(hand);
				if (!hand.getWork()) {
					System.out.println(hand);
					for (Ticket ticket : tickets) {
						if (ticket.status() == Ticket.Status.NEW  && ticket.getId() == Ticketid) {
							ticket.handle(hand);
							result = hand;
							hand.workAdd();
							break;
						}
					}
				}
				else {
					return null;
				}
			}
		}
		return result;
	}
}
